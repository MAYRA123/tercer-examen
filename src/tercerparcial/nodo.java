/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tercerparcial;

/**
 *
 * @author pc
 */
public class nodo {
   private T value;//valor el cual se va guardar
    private nodo<T> next;//apunta al siguiente nodo
    
    public nodo(T v, nodo<T> n){
        this.value = v;
        this.next = n;
    }

    public T getValue() { // retonar el valor que tengo guardado
        return value;
    }

    public void setValue(T value) {//modificar lo que es el valor que tenemos guardado
        this.value = value;
    }

    public nodo<T> getNext() {
        return next;
    }

    public void setNext(nodo<T> next) {
        this.next = next;
    }